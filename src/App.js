
const App = ({ notes }) => {
  console.log(notes)
  return (
    <div>
      <h1>notes</h1>
      <ul>
        {/* jsx */}
        {notes.map(item =>
          <li key={item.id}>{item.content}</li>
        )}
      </ul>
    </div>
  )
}

export default App