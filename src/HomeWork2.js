import { useState } from 'react'

//分值
const score = {
  good:1,
  neutral:0,
  bad:-1
}

const Button = ({text,callback}) => {
  return (
    <button onClick={callback}>{text}</button>
  )
}

const StatisticLine = ({text,value}) => {
  return (
  <>
    <tr>
      <td>{text}</td> 
      <td>{value}</td>
    </tr>
  </>)
}

const Statistics = ({good,neutral,bad}) =>{
  //对象结构
 // const {good,neutral,bad} = props
 if(good===0 && neutral===0 && bad===0){
      return (
      <>
        <h2>反馈值</h2>
        <p>还没有反馈</p>
      </>)  
  }
  return (
    <>
      <h2>反馈值</h2>
      <StatisticLine text="good" value={good}/>
      <StatisticLine text="neutral" value={neutral}/>
      <StatisticLine text="bad" value={bad}/>
      <StatisticLine text="all" value={good + neutral + bad}/>
      <StatisticLine text="平均分" value={(good*score.good + neutral*score.neutral + bad*score.bad)
        /(good+neutral+bad)}/>
      <StatisticLine text="积极反馈" value={good/(good+neutral+bad)*100+'%'}/>
    </>
  )
}

const App = () => {
  // save clicks of each button to its own state
  const [good, setGood] = useState(0)
  const [neutral, setNeutral] = useState(0)
  const [bad, setBad] = useState(0)

  //JSX
  return (
    <div>
      <h1>give freeback</h1>
      <Button text="good" callback={() => setGood(good+1)}/>
      <Button text="neutral" callback={() => setNeutral(neutral+1)}/>
      <Button text="bad" callback={() => setBad(bad+1)}/>
      <Statistics 
        good={good} 
        neutral={neutral}
        bad={bad}  
      />
    </div>
  )
}

export default App